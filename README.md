# DoctorHospital-iOS-SDK

## 蚂蚁云医-互联网医院SDK

### 方法调用

1、进入互联网医院：

        [DoctorSDK openHospital:phone partid:@"机构号" partcode:@"机构代码加密" uiview:UIViewController];
    
    （phone：加密后的手机号）
   
   
### 集成配置

 （一）、Podfile中添加：

    pod 'HospitalDoctor', :git => "https://gitee.com/mydrant/Hospital-Doctor.git",:tag => '2.1.2'
    
    （仅支持真机测试）


 （二）、其他Pod，若项目中无，则添加：
 
    注意添加源地址
 
    source 'https://github.com/CocoaPods/Specs.git'
    source 'https://github.com/aliyun/aliyun-specs.git'
    source 'https://cdn.cocoapods.org/'
   
    use_frameworks!
   
    pod 'JXBWebKit', '~> 1.3.0'
    pod 'AFNetworking', '~> 3.0', :subspecs => ['Reachability', 'Serialization', 'Security', 'NSURLSession']
    pod 'Masonry', '~> 1.0.1'
    pod 'MBProgressHUD', '~> 1.1.0'
    pod 'IQKeyboardManager'
    pod 'SVProgressHUD', '~> 2.2.5'
    pod 'AlicloudHTTPDNS', '~> 1.19.2.6'
    pod 'FMDB', '~> 2.7.5'
    pod 'MJExtension', '~> 3.2.2'
    pod 'GPUImage', '~> 0.1.7'
    pod 'SDWebImage', '~>3.8'
    pod 'MJRefresh'
    
 （二）、iOS-Demo：
  
    https://www.pgyer.com/7SFW
    (请联系技术添加UUID)


### 其他版本

 （一）、2.1.1版本：代码内部Pod版本不同

    pod 'HospitalDoctor', :git => "https://gitee.com/mydrant/Hospital-Doctor.git",:tag => '2.1.1'
    
    （仅支持真机测试）
    
    pod 'JXBWebKit', '~> 1.3.0'
    pod 'AFNetworking', '~> 4.0.1'
    pod 'Masonry', '~> 1.1.0'
    pod 'MBProgressHUD', '~> 1.2.0'
    pod 'IQKeyboardManager', '~> 6.5.6'
    pod 'SVProgressHUD', '~> 2.2.5'
    pod 'AlicloudHTTPDNS', '~> 1.19.2.6'
    pod 'FMDB', '~> 2.7.5'
    pod 'MJExtension', '~> 3.2.2'
    pod 'GPUImage', '~> 0.1.7'
    pod 'SDWebImage', '~>5.9.3'
    pod 'MJRefresh', '~>3.5.0'
    
    
